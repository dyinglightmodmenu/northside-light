<!--Please use Emojicopy (https://www.emojicopy.com) to insert Emoji's.-->

# 😱About this Mod💽

A Mod That Changes the Textures of the Game

Server for more outfits - https://discord.gg/r38KnKNcXW

#

<details><!--Installation-->
<summary><h2>🖥️ Installation 📩</h2></summary>

+   Download and install [x64 Installer](https://www.oracle.com/java/technologies/downloads/#java8-windows)

+   Extract the file

+   Launch resorep.jar
 
+   Click the "Add application" button

+   Find and Choose DyingLightGame.exe

+   Then Choose 64-bit
    
+   You can close the application now

+   Add the .dds files into the modded folder (the textures work only for certain texture qualities like medium or high)

+   Launch the game

#
</details><!--Installation-->

<details><!--Credits-->
<summary><h2>📒 Credits 📤</h2></summary>

- [Inari](https://gitlab.com/altyinari) (Who Put the Files Together)

#